"""
The Tag model
"""

from typing import List, TYPE_CHECKING, Union
from dataclasses import dataclass, field

from sqlalchemy import Table, Column, String, Integer, ForeignKey
from sqlalchemy.orm import relationship, backref
from slugify import slugify

from .base import Base, session, single_lookup
from .zettel import mapper_registry

if TYPE_CHECKING:
    from .zettel import Zettel

# Read more: https://docs.sqlalchemy.org/en/13/orm/basic_relationships.html#many-to-many
# And: https://docs.sqlalchemy.org/en/13/orm/extensions/associationproxy.html
join_table = Table("tag_zettel", mapper_registry.metadata,
    Column("zettel_id", Integer, ForeignKey("zettel.id")),
    Column("tag_id", Integer, ForeignKey("tag.id"))
)

@mapper_registry.mapped
@dataclass(repr=False)
class Tag(Base):
    """A tag"""
    __tablename__ = "tag"

    id: int = field(init=False, metadata={"sa": Column(Integer, primary_key=True)})
    name: str = field(metadata={"sa": Column(String(100))})
    slug: str = field(init=False, metadata={"sa": Column(String(100))})
    zettels: List = field(default_factory=list, metadata={"sa": relationship("Zettel",
        secondary=join_table,
        primaryjoin="Tag.id == tag_zettel.c.tag_id",
        secondaryjoin="Zettel.id == tag_zettel.c.zettel_id",
        backref=backref("tags", collection_class=set)
    )})

    def __post_init__(self) -> None:
        if not self.slug:
            self.slug = slugify(self.name)

    def __repr__(self):
        return self.slug

    @classmethod
    @single_lookup
    def by_name(cls, name: str) -> "Tag":
        """Get a tag by name"""
        return session.query(cls).filter_by(name=name).one()

    @classmethod
    @single_lookup
    def by_slug(cls, slug: str) -> "Tag":
        """Get a tag by slug"""
        return session.query(cls).filter_by(slug=slug).one()

    def to_json(self) -> dict:
        """Serialize this object"""
        return {
            "id": self.id,
            "name": self.name,
            "slug": self.slug,
        }

    def __hash__(self):
        return hash(self.name)


def tag_or_tag_name(item: Union[str, Tag]) -> Tag:
    """Allow functions to accept tags or tag names interchangeably"""
    if isinstance(item, str):
        return Tag.by_name(item)
    return item
