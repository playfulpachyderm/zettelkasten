"""
Handles DB connection, and gibberish with SQL Alchemy
"""

from functools import wraps
import inspect
import re

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound

from config import config

engine = create_engine("{}://{}:{}@{}:{}/{}".format(
    config["db"]["protocol"],
    config["db"]["username"],
    config["db"]["password"],
    config["db"]["hostname"],
    config["db"]["port"],
    config["db"]["database_name"]
))

session = sessionmaker(bind=engine)()


def single_lookup(f):
    """Decorator to add more information to `NoResultFound`s"""
    @wraps(f)
    def ret(cls, *args, **kwargs):
        try:
            return f(cls, *args, **kwargs)
        except (NoResultFound, MultipleResultsFound) as e:
            # Change the error message, leave everything else the same
            args_dict = dict(zip(inspect.getfullargspec(f).args[1:], args))
            new_msg = "{} such {}: {}".format(
                {NoResultFound: "No", MultipleResultsFound: "More than one"}[type(e)],
                cls.__name__,
                args_dict
            )
            e.args = (new_msg, ) + e.args[1:]
            raise

    return ret

def camel_to_snake(s):
    """Convert a camel-case string to snake-case"""
    return re.sub(r'(?<!^)(?=[A-Z])', '_', s).lower()

class Base:
    """A base model class with some generic methods"""
    __sa_dataclass_metadata_key__ = "sa"

    id: int = 0

    @classmethod
    def all(cls, order_by=None):
        """Get all instances of this type.  Default order by id"""
        if not order_by:
            order_by = cls.id
        return session.query(cls).order_by(order_by).all()

    @classmethod
    @single_lookup
    def by_id(cls, id):
        """Lookup an item by its id"""
        return session.query(cls).filter_by(id=id).one()

    @classmethod
    def where(cls, *args, **kwargs):
        """Simulate a `where` clause in SQL"""
        if args:
            return session.query(cls).filter(*args).all()
        else:
            return session.query(cls).filter_by(*args, **kwargs).all()

    def save(self):
        """Convenience method to save a model"""
        session.add(self)
        session.commit()

    @classmethod
    def url_path(cls, path=None):
        """The URL path that this model's API sits at"""
        base_path = "/{}".format(camel_to_snake(cls.__name__))
        if path is None:
            return base_path
        return "/".join([base_path, path])

    @classmethod
    def url_for(cls, item):
        """Get the appropriate link to a database item"""
        return "/".join([
            cls.url_path(),
            str(item.id)
        ])
