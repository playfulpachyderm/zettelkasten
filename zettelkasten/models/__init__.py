"""
Provide the model types
"""

from .base import session

from .zettel import Zettel
from .quote import Quote
from .link import Link

from .tag import Tag
