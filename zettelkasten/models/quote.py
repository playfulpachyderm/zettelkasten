"""
The Quote model
"""

from dataclasses import dataclass

from .zettel import Zettel, mapper_registry

@mapper_registry.mapped
@dataclass(init=True)
class Quote(Zettel):
    """A Quote zettel"""
    __mapper_args__ = {
        "polymorphic_identity": 1
    }

    def __post_init__(self) -> None:
        if self.text is None:
            raise ValueError("Quote must have a text")
        if self.author is None:
            raise ValueError("Quote must have an author")

    def to_json(self) -> dict:
        """Serialize this object"""
        return {
            "type": "quote",
            "id": self.id,
            "author": self.author,
            "text": self.text,
            "title": self.title,
            "url": self.url,
            "tags": [t.to_json() for t in self.tags]
        }
