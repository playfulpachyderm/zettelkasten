"""
The Link model
"""

from dataclasses import dataclass
import urllib.request as request

from bs4 import BeautifulSoup

from .zettel import Zettel, mapper_registry

@mapper_registry.mapped
@dataclass(init=True)
class Link(Zettel):
    """A Link zettel"""
    __mapper_args__ = {
        "polymorphic_identity": 2
    }

    def __post_init__(self) -> None:
        if self.url is None:
            raise ValueError("Link must have an URL")
        if self.title is None:
            raise ValueError("Link must have a title")

    def to_json(self) -> dict:
        """Serialize this object"""
        return {
            "type": "link",
            "id": self.id,
            "author": self.author,
            "text": self.text,
            "title": self.title,
            "url": self.url,
            "tags": [t.to_json() for t in self.tags]
        }

    @staticmethod
    def scrape_title(html: str) -> str:
        """Extract the `title` element from some html"""
        root = BeautifulSoup(html, "html.parser")
        return root.title.text

    def download_contents(self) -> str:
        """Download the contents of this link"""
        if isinstance(self.url, str):
            req = request.Request(self.url, headers={"user-agent": "curl/7.69.1", "accept": "*/*"})
            return request.urlopen(req).read().decode("latin-1")
        raise ValueError("This link has no URL for some reason")
