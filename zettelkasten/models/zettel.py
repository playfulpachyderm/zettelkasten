"""
The base Zettel model for all zettel-types
"""

from typing import Set, Optional, Union, ClassVar
from dataclasses import dataclass, field

from sqlalchemy import Column, String, Integer, or_
from sqlalchemy.schema import FetchedValue
from sqlalchemy.orm import registry
mapper_registry: registry = registry()

from .base import Base
from .tag import Tag, tag_or_tag_name


@mapper_registry.mapped
@dataclass(init=False)
class Zettel(Base):
    """Base class for all the zettel types"""
    __tablename__ = "zettel"

    __mapper_args__: ClassVar = {
        "polymorphic_on": "type",
        "eager_defaults": True,
    }

    id: int = field(init=False, metadata={"sa": Column(Integer, primary_key=True)})
    type: int = field(init=False, metadata={"sa": Column(Integer)})
    timestamp: Optional[int] = field(default=None, metadata={"sa": Column(Integer)})

    title: Optional[str] = field(default=None, metadata={"sa": Column(String)})
    url: Optional[str] = field(default=None, metadata={"sa": Column(String)})

    author: Optional[str] = field(default=None, metadata={"sa": Column(String)})
    text: Optional[str] = field(default=None, metadata={"sa": Column(String)})

    tags: Set[Tag] = field(default_factory=set)  # backref

    full_text_vector: str = field(init=False, repr=False,
        metadata={"sa": Column(String, server_default=FetchedValue())}
    )

    def __init__(self, *args, **kwargs) -> None:
        raise TypeError("Cannot be instantiated")

    def tag_with(self, tag: Union[str, Tag]) -> None:
        """Tag this zettel"""
        _tag: Tag = tag_or_tag_name(tag)

        if _tag in self.tags:
            raise ValueError("Already tagged with that tag")

        self.tags.add(_tag)

    def untag_with(self, tag: Union[str, Tag]) -> None:
        """Untag this zettel"""
        _tag: Tag = tag_or_tag_name(tag)

        if _tag not in self.tags:
            raise ValueError("Not tagged with that tag")

        self.tags.remove(_tag)


    @classmethod
    def search(cls, text):
        """Full text search of all zettels"""
        assembled = " & ".join(text.split(" "))
        return cls.where(or_(
            cls.full_text_vector.match(assembled),
            cls.url.ilike(f"%{text}%")
        ))
