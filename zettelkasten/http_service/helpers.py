"""
Some helper functions for controllers to use
"""

from flask import abort
from sqlalchemy.exc import NoResultFound


def get_or_404(cls: type, id: int):
    """Return an item by id, or raise a 404 if it doesn't exist"""
    try:
        return cls.by_id(id)  # type: ignore
    except NoResultFound:
        abort(404)


def get_by_slug_or_404(cls: type, slug: str):
    """Return an item by slug, or raise a 404 if it doesn't exist"""
    try:
        return cls.by_slug(slug)  # type: ignore
    except NoResultFound:
        abort(404)
