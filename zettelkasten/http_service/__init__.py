"""
Generate and configure a Flask application, based on the loaded config
"""

import os

from flask import Flask

from config import config
from models import Tag
from .webpages import webpages_blueprint
from .api import quotes_blueprint, links_blueprint, tags_blueprint, search_blueprint


app = Flask(
    __name__,
    template_folder=os.path.join(os.path.dirname(os.path.dirname(__file__)), "views")
)
app.config.update(config)
app.url_map.strict_slashes = app.config["strict_slashes"]

app.register_blueprint(webpages_blueprint, url_prefix="/")
app.register_blueprint(quotes_blueprint, url_prefix="/api/quotes")
app.register_blueprint(links_blueprint, url_prefix="/api/links")
app.register_blueprint(tags_blueprint, url_prefix="/api/tags")
app.register_blueprint(search_blueprint, url_prefix="/api/search")


# Template helper functions
# -------------------------

@app.template_filter('static_url')
def static_url_for(s):
    """
    Jinja helper function to generate a full url for a static asset by joining it with the
    static root URL
    """
    return app.config["static_root_url"] + s

@app.template_filter('title_for')
def title_for(s):
    """
    Jinja helper function to generate a full url for a static asset by joining it with the
    static root URL
    """
    if s:
        return f"{s} | Zettelkasten"
    return "Zettelkasten"

@app.context_processor
def get_tags():
    """Build a dictionary to be added to the template context"""
    def tags():
        return Tag.all()
    return dict(tags=tags)
