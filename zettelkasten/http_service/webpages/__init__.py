"""
Render some webpages
"""

import os

from flask import Blueprint, render_template

from models import Quote, Link, Tag
from http_service.helpers import get_or_404, get_by_slug_or_404

webpages_blueprint = Blueprint('webpages', __name__,
    template_folder="templates"
)

@webpages_blueprint.route("/")
def index():
    """The homepage"""
    return render_template("index.html")


# Quotes pages
# ------------

@webpages_blueprint.route("/quotes")
def quotes_index():
    """Quotes index page"""
    return render_template("quotes/index_quotes.html")

@webpages_blueprint.route("/quotes/<int:quote_id>")
def quotes_show(quote_id):
    """Show a single quote"""
    q = get_or_404(Quote, quote_id)
    title = q.author
    if q.title:
        title += ": " + q.title
    return render_template("quotes/show_quote.html", title=title)


@webpages_blueprint.route("/quotes/new")
def quotes_new_form():
    """Form to create a new quote"""
    return render_template("quotes/new_quote.html")


# Links pages
# -----------

@webpages_blueprint.route("/links")
def links_index():
    """Links index page"""
    return render_template("links/index_links.html")

@webpages_blueprint.route("/links/<int:link_id>")
def links_show(link_id):
    """Show a single link"""
    l = get_or_404(Link, link_id)
    return render_template("links/show_link.html", title=l.title)


@webpages_blueprint.route("/links/new")
def links_new_form():
    """Form to create a new link"""
    return render_template("links/new_link.html")


# Tags pages
# -----------

@webpages_blueprint.route("/tags/<string:slug>")
def tag_show(slug):
    """Show a tag"""
    tag = get_by_slug_or_404(Tag, slug)
    return render_template("tags/show_tag.html", title=tag.name)


# Search page
# -----------

@webpages_blueprint.route("/search")
def show_search_results():
    """Show the results of a search query"""
    return render_template("search.html", title="Search")
