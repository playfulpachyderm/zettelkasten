"""JSON API for Tags"""

from flask import Blueprint, jsonify, request

from models import Tag, Zettel

tags_blueprint = Blueprint('tags_api', __name__)

@tags_blueprint.route("/")
def index_tags():
    """Get all the tags"""
    return jsonify([t.to_json() for t in Tag.all()])

@tags_blueprint.route("/<string:slug>")
def show_tag(slug):
    """Show a tag"""
    tag = Tag.by_slug(slug)
    ret = tag.to_json()

    if "with-zettels" in request.args:
        ret["zettels"] = [z.to_json() for z in tag.zettels]

    return jsonify(ret)

@tags_blueprint.route("/<string:slug>/zettels/<int:zettel_id>", methods=["POST"])
def tag_zettel(slug, zettel_id):
    """Tag a zettel with the given tag"""
    z = Zettel.by_id(zettel_id)
    tag = Tag.by_slug(slug)

    try:
        z.tag_with(tag)
    except ValueError as e:
        return jsonify({"error": str(e)}), 422

    z.save()
    return jsonify(z.to_json())

@tags_blueprint.route("/<string:slug>/zettels/<int:zettel_id>", methods=["DELETE"])
def untag_zettel(slug, zettel_id):
    """Untag a zettel, removing the given tag"""
    z = Zettel.by_id(zettel_id)
    tag = Tag.by_slug(slug)

    try:
        z.untag_with(tag)
    except ValueError as e:
        return jsonify({"error": str(e)}), 422

    z.save()
    return jsonify(z.to_json())
