"""JSON API for Links"""

from flask import Blueprint, jsonify, request

from models import Link
from http_service.helpers import get_or_404

links_blueprint = Blueprint('links_api', __name__)


@links_blueprint.route("/")
def index_links():
    """Get all the links"""
    return jsonify([l.to_json() for l in Link.all()])

@links_blueprint.route("/<int:link_id>")
def show_link(link_id):
    """Get a link by id"""
    return jsonify(get_or_404(Link, link_id).to_json())

@links_blueprint.route("/new", methods=["POST"])
def new_link():
    """Create a new link"""
    data = request.json
    if "url" not in data:
        return jsonify({"errors": "Missing required field: url"}), 422

    l = Link(
        url=data["url"],
        title="",
        author=data.get("author"),
        text=data.get("text")
    )
    l.title = Link.scrape_title(l.download_contents())
    l.save()
    return jsonify(l.to_json())
