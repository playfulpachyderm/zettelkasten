"""Blueprints for JSON APIs"""

from .quotes_api import quotes_blueprint
from .links_api import links_blueprint
from .tags_api import tags_blueprint
from .search_api import search_blueprint
