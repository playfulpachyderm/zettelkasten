"""JSON API for Quotes"""

from flask import Blueprint, jsonify, request

from models import Quote
from http_service.helpers import get_or_404

quotes_blueprint = Blueprint('quotes_api', __name__)


@quotes_blueprint.route("/")
def index_quotes():
    """Get all the quotes"""
    where = []
    if "author" in request.args:
        where.append(Quote.author.ilike("%{author}%".format(**request.args)))
    quotes = Quote.where(*where)
    return jsonify([q.to_json() for q in quotes])

@quotes_blueprint.route("/<int:quote_id>")
def show_quote(quote_id):
    """Get a quote by id"""
    return jsonify(get_or_404(Quote, quote_id).to_json())

@quotes_blueprint.route("/new", methods=["POST"])
def new_quote():
    """Create a new quote"""
    data = request.json
    missing_data = []
    for required in "text", "author":
        if required not in data:
            missing_data.append(f"Missing required field: {required}")
    if missing_data:
        return jsonify({"errors": missing_data}), 422

    q = Quote(
        author=data["author"],
        text=data["text"],
        title=data.get("title", None),
        url=data.get("url", None)
    )
    q.save()
    return jsonify(q.to_json())
