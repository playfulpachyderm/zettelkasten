"""JSON API for Tags"""

from flask import Blueprint, jsonify, request, abort

from models import Zettel

search_blueprint = Blueprint('search_api', __name__)

@search_blueprint.route("/")
def search():
    """Get search results"""
    query = request.args.get("search-query")
    if not query:
        abort(422)
    results = Zettel.search(query)
    return jsonify([z.to_json() for z in results])
