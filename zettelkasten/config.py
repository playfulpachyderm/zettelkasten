"""
Generate a config for the Flask application.

Environment is given by the contents of the `.environment` file, since apparently
environment variables do not play nice with Apache and mod_wsgi.  Was unable to
find any way to pass a custom environment variable like ENVIRONMENT through APache.
"""

import os
import json

ROOT_DIR = os.path.dirname(__file__)

def load_value_from_file(filename):
    """Read a file and return its contents"""
    abs_path = os.path.join(ROOT_DIR, filename)
    return open(abs_path).read().strip()

def determine_environment():
    """
    1) Test `ENVIRONMENT` environment variable
    2) check for `.environment` file
    3) assume dev
    """
    try:
        print("Trying ENVIRONMENT variable...")
        return os.environ["ENVIRONMENT"]
    except KeyError:
        pass

    try:
        print("Trying `.environment` file...")
        return load_value_from_file(".environment")
    except FileNotFoundError:
        pass

    print("Defaulting to dev")

    return "dev"

def config_for_env(env):
    """Set the config parameters based on the given environment and return a config dictionary"""
    if env not in ["dev", "test", "prod"]:
        raise ValueError(env)

    print(f"=================== Starting up in environment: {env} =================== ")

    if env == "prod":
        try:
            db_password = load_value_from_file(".prod_password")
        except FileNotFoundError as e:
            raise RuntimeError("ERROR could not load production password!!") from e
        app_login = {
            "salt": "5p/ifYBg49YJUQ==",
            "hex_digest": "0dde338cb7ede98fed170b2550eb0962929e6eec716db5ec2f51073c20af6b63",
        }
    else:
        db_password = "zk"
        app_login = {
            "salt": "/6XL6Atnr4dcJA==",
            "hex_digest": '6bbff3f09fa60f03e045a5bdf1d5308c1e280f94753d4fa2e10b56e452e213c8',
        }

    return {
        "ROOT_DIR": ROOT_DIR,
        "ENVIRONMENT": env,
        "db": {
            # Priority:
            # 1) specific environment variable (e.g., `PG_HOST`)
            # 2) environment default
            # 3) default value
            "protocol": "postgresql+psycopg2",
            "username": "zk",
            "hostname": os.environ.get("PG_HOST", "localhost"),
            "port": os.environ.get("PG_PORT", 5433 if env == "test" else 5432),
            "database_name": os.environ.get("PG_DATABASE", "zk"),
            "password": db_password
        },
        "secret_key": os.urandom(20).decode("latin1"),
        "app_login": app_login,

        "EXPLAIN_TEMPLATE_LOADING": env != "prod",
        "DEBUG": env != "prod",

        "static_root_url": {
            "dev": "",
            "test": "test://epicserver.com",
            "prod": "https://playfulpachyderm.s3-us-west-2.amazonaws.com/food",
        }[env],

        "strict_slashes": False,
    }

environment = determine_environment()
config = config_for_env(environment)

if config["ENVIRONMENT"] != "prod":
    print(json.dumps(config, indent=4))
