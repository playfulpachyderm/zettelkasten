#!/sbin/openrc-run

name="uWSGI: Zettelkasten"
command="/usr/bin/uwsgi"
command_args="-d --yaml /app/zettelkasten/zettelkasten-uwsgi-config.yaml"
pidfile="/run/uwsgi-zettelkasten.pid"

depend() {
    provide zettelkasten-uwsgi
}
