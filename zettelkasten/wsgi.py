"""
Make the WSGI application available for importing
"""

import sys
sys.dont_write_bytecode = True

from http_service import app as application  # pylint: disable=unused-import, wrong-import-position
