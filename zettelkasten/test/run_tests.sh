#!/usr/bin/env sh

set -e
set -x

base_path=$(readlink -f $0 | xargs dirname | xargs dirname)

if [[ "$1" == "with-coverage" ]]
then
    shift
    PYTHONPATH=$base_path ENVIRONMENT="test" pytest --cov=models --cov=http_service --cov-report term-missing $@
else
    PYTHONPATH=$base_path ENVIRONMENT="test" pytest $@
fi
