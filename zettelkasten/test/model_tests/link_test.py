"""Tests of the Quote model"""

from pytest import raises
from models import Link, Tag

def test_can_create_new_link() -> None:
    """Should create a new Link"""
    l = Link(text="Test text", author="Test author", title="Test title", url="Test url")
    assert l.text == "Test text"
    assert l.author == "Test author"
    assert l.title == "Test title"
    assert l.url == "Test url"

def test_can_create_new_link_without_author_or_text() -> None:
    """Author and text fields should be optional"""
    l = Link(url="test url", title="Test title")
    assert l.url == "test url"
    assert l.title == "Test title"
    assert l.author is None
    assert l.text is None

def test_link_must_have_title_and_url() -> None:
    """Title and URL fields are required"""
    with raises(ValueError):
        Link(title="wefjk")  # Must have an URL
    with raises(ValueError):
        Link(url="wfejk")    # must have a title


def test_to_json(link: Link, tag: Tag) -> None:
    """Should serialize correctly"""
    json = link.to_json()

    assert json["url"] == "https://playfulpachyderm.com/potatoes"
    assert json["title"] == "Potatoes | Playful Pachyderm"
    assert json["id"] == 823
    assert json["author"] is None
    assert json["text"] is None
    assert json["type"] == "link"

    assert len(json["tags"]) == 1
    assert json["tags"][0] == tag.to_json()

def test_scrape_title(html_body: str) -> None:
    """Should extract the `<title>` element's text contents"""
    assert Link.scrape_title(html_body) == "Some stuff"

def test_extract_title_from_link(link: Link) -> None:
    """Should download a url and extract the title"""
    assert Link.scrape_title(link.download_contents()) == "Some stuff"
