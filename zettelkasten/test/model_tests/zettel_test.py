"""Tests of the Zettel model and the Tag-Zettel relationship"""

from pytest import raises
from models import Zettel, Tag, Quote, Link

def test_zettel_works() -> None:
    """Should get a zettel"""
    z = Zettel.all()[0]
    assert isinstance(z, Zettel)
    assert z.id

def test_relationship_works(quote: Quote, tag: Tag) -> None:
    """Should be able to cross-reference tags and zettels"""
    assert quote not in tag.zettels
    assert tag not in quote.tags

    quote.tags.add(tag)

    assert quote in tag.zettels
    assert tag in quote.tags

def test_cant_instantiate_zettel() -> None:
    """Shouldn't be able to create a new Zettel without a more specific type"""
    with raises(TypeError):
        Zettel(title="Asdf", text="wefjk")


# Tagging / untagging
# -------------------

def test_tag_with(quote: Quote, tag: Tag) -> None:
    """Should add a tag to the zettel, and add the zettel to the tag"""
    assert tag not in quote.tags
    assert quote not in tag.zettels
    length = len(quote.tags)

    quote.tag_with(tag)

    assert tag in quote.tags
    assert quote in tag.zettels
    assert len(quote.tags) == length + 1

def test_tag_with_duplicate_tag(link: Link, tag: Tag) -> None:
    """Should refuse to add a tag if it's already tagged with that"""
    assert tag in link.tags
    with raises(ValueError):
        link.tag_with(tag)

def test_tag_with_by_string() -> None:
    """Tag a zettel by the tag name, instead of a Tag object"""
    z = Zettel.all()[0]
    assert z not in Tag.by_name("pain").zettels
    z.tag_with("pain")

    assert z in Tag.by_name("pain").zettels

def test_untag_with(link: Link, tag: Tag) -> None:
    """Should remove the tag from the zettel"""
    assert tag in link.tags
    assert link in tag.zettels
    length = len(link.tags)

    link.untag_with(tag)

    assert tag not in link.tags
    assert len(link.tags) == length - 1
    assert link not in tag.zettels

def test_untag_with_nonexistent_tag(quote: Quote, tag: Tag) -> None:
    """Should fail to remove a tag that doesn't exist"""
    assert tag not in quote.tags
    with raises(ValueError):
        quote.untag_with(tag)

def test_untag_with_by_string() -> None:
    """Tag a zettel by the tag name, instead of a Tag object"""
    z = Zettel.by_id(5)
    t = Tag.by_name("pain")
    assert t in z.tags

    z.untag_with("pain")

    assert z not in t.zettels
    assert t not in z.tags



# Full text search
# ----------------

def test_full_text_search() -> None:
    """Should be searchable by any field"""
    assert Zettel.search("library")[0].id == 18
    assert Zettel.search("structure")[0].id == 18
    assert Zettel.search("ionelmc")[0].id == 18
    assert Zettel.search("tox")[0].id == 18

def test_full_text_search_no_results() -> None:
    """Should return an empty array"""
    assert Zettel.search("fwlkefjwgaw") == []

def test_full_text_search_should_be_case_insensitive() -> None:
    """Should not be case sensitive"""
    zettels = Zettel.search("SINGSING")
    assert len(zettels) == 1
    assert zettels[0].id == 17

    zettels = Zettel.search("singsing")
    assert len(zettels) == 1
    assert zettels[0].id == 17

def test_full_text_search_multiple_words() -> None:
    """Should accept a list of search terms"""
    zettels = Zettel.search("anne frank")
    assert len(zettels) == 1
    assert zettels[0].id == 17
