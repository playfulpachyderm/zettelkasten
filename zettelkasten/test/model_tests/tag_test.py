"""Tests of the Tag model"""

from models import Tag, Zettel

def test_tag_works() -> None:
    """Basic SQLAlchemy setup"""
    t = Tag.all()[0]
    assert isinstance(t, Tag)
    assert t.id
    assert len(t.zettels) != 0
    assert isinstance(t.zettels[0], Zettel)

def test_tag_by_name() -> None:
    """Should find a tag by name"""
    t = Tag.by_name("pain")
    assert t.name == "pain"

def test_tag_by_slug() -> None:
    """Should find a tag by slug"""
    t = Tag.by_slug("chess-advice")
    assert t.name == "chess advice"

def test_create_new_tag() -> None:
    """Should create a new tag"""
    name = "fawjelgg gwek"
    slug = "sodijfw"
    t = Tag(name)
    t.slug = slug
    assert t.name == name
    assert t.slug == slug

def test_create_new_tag_with_implicit_slug() -> None:
    """Should create a new tag"""
    name = "fawjelgg gwek"
    t = Tag(name)
    assert t.name == name
    assert t.slug == "fawjelgg-gwek"


def test_to_json(tag) -> None:
    """Should serialize correctly"""
    json = tag.to_json()

    assert json["id"] == 142
    assert json["name"] == "test tag"
    assert json["slug"] == "test-tag"
