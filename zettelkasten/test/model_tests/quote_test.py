"""Tests of the Quote model"""

from pytest import raises
from models import Quote

def test_can_create_new_quote() -> None:
    """Should create a new Quote"""
    q = Quote(text="Test text", author="Test author", title="Test title", url="Test url")
    assert q.text == "Test text"
    assert q.author == "Test author"
    assert q.title == "Test title"
    assert q.url == "Test url"

def test_can_create_new_quote_without_title_or_url() -> None:
    """Title and URL fields should be optional"""
    q = Quote(text="Test text", author="Test author")
    assert q.text == "Test text"
    assert q.author == "Test author"
    assert q.title is None
    assert q.url is None

def test_quote_must_have_author_and_text() -> None:
    """Text and author fields are required"""
    with raises(ValueError):
        Quote(text="Test text")  # Must have an author
    with raises(ValueError):
        Quote(author="Test author")  # Must have text


def test_to_json(quote: Quote) -> None:
    """Should serialize correctly"""
    json = quote.to_json()

    assert json["text"] == "Blah blah blah"
    assert json["author"] == "Some dude"
    assert json["title"] == "Comments on life"
    assert json["url"] is None
    assert json["type"] == "quote"
    assert json["id"] == 123

    assert json["tags"] == []
