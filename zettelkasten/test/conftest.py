"""
Configuration for tests
"""

import sys
sys.dont_write_bytecode = True

import urllib.request
from io import BytesIO
from typing import Union

from pytest import fixture

from models import Quote, Tag, Link

@fixture
def quote() -> Quote:
    """Fixture quote"""
    q = Quote(text="Blah blah blah", author="Some dude", title="Comments on life")
    # q.url = "https://twatter.com/dude/comment1"
    q.id = 123
    return q

@fixture
def tag() -> Tag:
    """Fixture tag"""
    t = Tag(name="test tag")
    t.id = 142
    return t

@fixture
def link(tag) -> Link:  # pylint: disable=redefined-outer-name
    """Fixture link"""
    l = Link(url="https://playfulpachyderm.com/potatoes", title="Potatoes | Playful Pachyderm")
    l.id = 823
    l.tag_with(tag)
    return l


@fixture
def html_body() -> str:
    """Fixture html page for a link"""
    return """<html><head><title>Some stuff</title></head><body>morestuff</body></html>"""


# Monkeypatch the `urllib.requests.urlopen` function
def fake_urlopen(_: Union[str, urllib.request.Request]) -> BytesIO:
    """Produce an object that has `.read()` that returns bytes"""
    return BytesIO(b"""<html><head><title>Some stuff</title></head><body>morestuff</body></html>""")

urllib.request.urlopen = fake_urlopen  # type: ignore
