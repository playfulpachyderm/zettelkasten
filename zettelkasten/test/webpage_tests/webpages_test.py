"""
Very basic tests of webpages
"""

from http_service import app

client = app.test_client()

def test_index():
    """Get the website home page"""
    page = client.get("/")
    assert page.status_code == 200


# Quotes
# ------

def test_quotes_index():
    """Get the quotes index page"""
    page = client.get("/quotes")
    assert page.status_code == 200

def test_quotes_show():
    """Get a quote page"""
    page = client.get("/quotes/1")
    assert page.status_code == 200

def test_quotes_show_404():
    """Nonexistent quotes should 404"""
    page = client.get("/quotes/234298532985345")
    assert page.status_code == 404

def test_quotes_new_form():
    """Get the new quote form"""
    page = client.get("/quotes/new")
    assert page.status_code == 200


# Links
# -----

def test_links_index():
    """Get the links index page"""
    page = client.get("/links/new")
    assert page.status_code == 200

def test_links_new_form():
    """Get the new link form"""
    page = client.get("/links")
    assert page.status_code == 200


# Tags
# -----

def test_tags_show():
    """Get a tag"""
    page = client.get("/tags/chess-advice")
    assert page.status_code == 200
