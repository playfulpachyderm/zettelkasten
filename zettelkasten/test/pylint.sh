#!/usr/bin/env sh

set -e
set -x

pylint *.py models http_service test
