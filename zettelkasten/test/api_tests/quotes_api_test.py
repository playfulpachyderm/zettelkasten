"""Tests for `/api/quotes`"""

from models import Quote
from http_service import app

client = app.test_client()

def test_index():
    """Should list the quotes"""
    page = client.get("/api/quotes")
    assert page.status_code == 200

    data = page.json
    assert len(data) == len(Quote.all())
    quote_data = data[0]
    q = Quote.by_id(quote_data["id"])
    assert q.to_json() == quote_data

def test_index_with_filtering_by_author():
    """Should filter the quotes"""
    query_string = {"author": "singsing"}
    page = client.get("/api/quotes", query_string=query_string)
    assert page.status_code == 200

    data = page.json
    assert len(data) == 1
    assert "Anne Frank" in data[0]["text"]

def test_show_quote():
    """Should return a single quote"""
    page = client.get("/api/quotes/5")
    assert page.status_code == 200

    assert page.json == Quote.by_id(5).to_json()

def test_show_nonexistent_quote():
    """Should 404"""
    page = client.get("/api/quotes/523452353")
    assert page.status_code == 404

def test_new_quote():
    """Should create a new quote"""
    json = {"author": "Fake Author", "text": "Fake Text"}
    page = client.post("/api/quotes/new", json=json)
    assert page.status_code == 200
    response = page.json

    q = Quote.by_id(response["id"])
    assert q.author == "Fake Author"
    assert q.text == "Fake Text"

def test_new_quote_without_author_or_text():
    """Creating a new quote without some required fields should fail"""
    page = client.post("/api/quotes/new", json={})
    assert page.status_code == 422

    assert "errors" in page.json
    assert len(page.json["errors"]) == 2
