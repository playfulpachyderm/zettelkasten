"""Tests for `/api/search`"""

from http_service import app

client = app.test_client()


def test_basic_search():
    """Search for a search term"""
    page = client.get("/api/search", query_string={"search-query": "singsing"})
    assert page.status_code == 200

    data = page.json
    assert isinstance(data, list)
    assert len(data) == 1
    assert data[0]["id"] == 17

def test_multiword_search():
    """Search for a search phrase"""
    page = client.get("/api/search", query_string={"search-query": "anne frank"})
    assert page.status_code == 200

    data = page.json
    assert isinstance(data, list)
    assert len(data) == 1
    assert data[0]["id"] == 17

def test_empty_search():
    """Search for empty string"""
    page = client.get("/api/search", query_string={"search-query": ""})
    assert page.status_code == 422

def test_search_for_nonsense():
    """Search for nonsense"""
    page = client.get("/api/search", query_string={"search-query": "haelrflskcsdew"})
    assert page.status_code == 200

    data = page.json
    assert isinstance(data, list)
    assert len(data) == 0
