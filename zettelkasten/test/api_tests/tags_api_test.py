"""Tests for `/api/tags`"""

from models import Tag, Zettel
from http_service import app

client = app.test_client()

def test_index():
    """Should list the tags"""
    page = client.get("/api/tags")
    assert page.status_code == 200

    data = page.json
    assert len(data) == len(Tag.all())
    tag_data = data[0]
    t = Tag.by_id(tag_data["id"])
    assert t.to_json() == tag_data

def test_show_tag():
    """Should get a tag"""
    page = client.get("/api/tags/chess-advice")
    assert page.status_code == 200
    data = page.json

    assert data["id"] == 1
    assert data["name"] == "chess advice"
    assert data["slug"] == "chess-advice"
    assert "zettels" not in data

def test_show_tag_with_zettels():
    """Should get a tag and its zettels"""
    page = client.get("/api/tags/chess-advice?with-zettels")
    assert page.status_code == 200
    data = page.json

    tag = Tag.by_slug("chess-advice")

    assert "zettels" in data
    assert len(data["zettels"]) == len(tag.zettels)
    for z in tag.zettels:
        assert z.to_json() in data["zettels"]


# Tagging / untagging
# -------------------

def test_tag_zettel():
    """Should add a tag to the zettel"""
    zettel = Zettel.by_id(5)  # "Never walk into a fork"
    tag_count = len(zettel.tags)
    page = client.post("/api/tags/dev/zettels/5")
    assert page.status_code == 200
    assert len(zettel.tags) == tag_count + 1

    # Teardown
    zettel.untag_with("dev")
    zettel.save()

def test_tag_zettel_with_existing_tag():
    """Should fail if the tag already exists"""
    zettel = Zettel.by_id(5)  # "Never walk into a fork"
    assert Tag.by_name("chess advice") in zettel.tags

    page = client.post("/api/tags/chess-advice/zettels/5")
    assert page.status_code == 422


def test_untag_zettel():
    """Should remove the tag from the zettel"""
    zettel = Zettel.by_id(5)  # "Never walk into a fork"
    tag_count = len(zettel.tags)
    page = client.delete("/api/tags/chess-advice/zettels/5")
    assert page.status_code == 200
    assert len(zettel.tags) == tag_count - 1

    # Teardown
    zettel.tag_with("chess advice")
    zettel.save()

def test_untag_zettel_with_nonexistent_tag():
    """Should produce an error if the tag doesn't exist"""
    zettel = Zettel.by_id(5)  # "Never walk into a fork"
    assert Tag.by_name("dev") not in zettel.tags

    page = client.delete("/api/tags/dev/zettels/5")
    assert page.status_code == 422
