"""Tests for `/api/links`"""

from models import Link
from http_service import app

client = app.test_client()

def test_index():
    """Should list the links"""
    page = client.get("/api/links")
    assert page.status_code == 200

    data = page.json
    assert len(data) == len(Link.all())
    link_data = data[0]
    l = Link.by_id(link_data["id"])
    assert l.to_json() == link_data

def test_show_link():
    """Should return a single link"""
    page = client.get("/api/links/20")
    assert page.status_code == 200

    assert page.json == Link.by_id(20).to_json()

def test_show_nonexistent_link():
    """Should 404"""
    page = client.get("/api/links/523452353")
    assert page.status_code == 404

def test_new_link():
    """Should create a new link"""
    json = {"url": "http://FakeUrl"}
    page = client.post("/api/links/new", json=json)
    assert page.status_code == 200
    response = page.json

    l = Link.by_id(response["id"])
    assert l.url == "http://FakeUrl"
    assert l.title == "Some stuff"

def test_new_link_without_author_or_text():
    """Creating a new link without an url should fail"""
    page = client.post("/api/links/new", json={})
    assert page.status_code == 422

    assert "errors" in page.json
