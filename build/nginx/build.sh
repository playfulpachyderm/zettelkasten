#!/bin/sh

THIS_DIR=$(readlink -f $0 | xargs dirname)
APP_ROOT_DIR=$(dirname $THIS_DIR | xargs dirname)

docker build -f $THIS_DIR/Dockerfile -t zettelkasten/nginx $THIS_DIR
