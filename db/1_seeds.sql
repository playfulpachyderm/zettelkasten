insert into tag (name, slug) values ('chess advice', 'chess-advice');
insert into tag (name, slug) values ('pain', 'pain');
insert into tag (name, slug) values ('dev', 'dev');
insert into tag (name, slug) values ('python', 'python');
insert into tag (name, slug) values ('lindy', 'lindy');


insert into zettel (type, author, text) values (1, 'Ben Finegold', 'The more you cheat, the more you win.');
insert into zettel (type, author, text) values (1, 'Ben Finegold', 'Trying is the first step to failure.');
insert into zettel (type, author, text) values (1, 'Ben Finegold', 'I say "Trying is the first step to failure" a lot.  More than anybody.');
insert into zettel (type, author, text) values (1, 'Ben Finegold', '"You miss 100% of the shots you don''t take"?  I don''t.');
insert into zettel (type, author, text) values (1, 'Ben Finegold', 'In real life and in chess, never walk into a fork.');
insert into tag_zettel(zettel_id, tag_id) values (5, 1);
insert into tag_zettel(zettel_id, tag_id) values (5, 2);
insert into zettel (type, author, text) values (1, 'Ben Finegold', 'Always play Bf1.');
insert into tag_zettel(zettel_id, tag_id) values (6, 1);
insert into zettel (type, author, text) values (1, 'Ben Finegold', 'Always play Kb1.');
insert into tag_zettel(zettel_id, tag_id) values (7, 1);
insert into zettel (type, author, text) values (1, 'Ben Finegold', 'Knife f5.');
insert into zettel (type, author, text) values (1, 'Ben Finegold', 'You have to have a king in chess, otherwise you lose.');
insert into tag_zettel(zettel_id, tag_id) values (9, 1);
insert into zettel (type, author, text) values (1, 'Ben Finegold', 'The truth hurts.');
insert into tag_zettel(zettel_id, tag_id) values (10, 2);
insert into zettel (type, author, text) values (1, 'Ben Finegold', 'The important thing is to believe everything you see and hear, then complain about it.');
insert into zettel (type, author, text) values (1, 'Ben Finegold', 'They played 30 games and Yifan didn''t win any of them. It''s better to win at least one.');
insert into zettel (type, author, text) values (1, 'Ben Finegold', 'A dollar a day keeps the IRS away.  Because they''re like, "I don''t want that."');
insert into zettel (type, author, text) values (1, 'Ben Finegold', 'Alright, maybe my king should go back to e3 for safety.');
insert into zettel (type, author, text) values (1, 'Ben Finegold', 'You''re the worst person in your chair.');
insert into zettel (type, author, text) values (1, 'Ben Finegold', '"Resigns" is the best move here.  Rf8 and Rf4 seem like they''re tied for second.');
insert into tag_zettel(zettel_id, tag_id) values (16, 2);

insert into zettel(type, author, text) values (1, 'Singsing', 'You''re useless!  Like Anne Frank''s drum kit.');


insert into zettel(type, url, title, text) values (2, 'https://blog.ionelmc.ro/2014/05/25/python-packaging', 'Packaging a python library', 'How to structure a python project, including packaging.  I think I found it because I was googling about \"tox\".');
insert into tag_zettel (zettel_id, tag_id) values (18, 3);
insert into tag_zettel (zettel_id, tag_id) values (18, 4);

insert into zettel(type, url, title, text) values (2, 'https://ivergara.github.io/ABC-and-dataclasses.html', 'Having fun with dataclasses and abstract base classes', 'Dataclasses, abstract base classes, and how to use them');
insert into tag_zettel (zettel_id, tag_id) values (19, 3);
insert into tag_zettel (zettel_id, tag_id) values (19, 4);

insert into zettel(type, url, title, text) values (2, 'https://medium.com/incerto/an-expert-called-lindy-fdb30f146eaf', 'An Expert Called Lindy', 'Nassim Taleb explains Lindy');
insert into tag_zettel (zettel_id, tag_id) values (20, 5);

insert into zettel(type, title, text, author) values (1, 'Larry Wall on Python', 'Perl is worse than Python because people wanted it worse.', 'Larry Wall');
insert into tag_zettel (zettel_id, tag_id) values (21, 4);
