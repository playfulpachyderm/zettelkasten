\set ON_ERROR_STOP 1

create table zettel_type (id serial primary key,
    name varchar(30)
);

insert into zettel_type (id, name) values (1, 'quote');
insert into zettel_type (id, name) values (2, 'link');
insert into zettel_type (id, name) values (3, 'media');
insert into zettel_type (id, name) values (4, 'post');
insert into zettel_type (id, name) values (5, 'todo');

create table zettel (id serial primary key,
    created_at timestamp not null default now(),
    updated_at timestamp,

    type integer references zettel_type not null,

    timestamp timestamp,
    url varchar(300),
    author varchar(50),
    title varchar(200),
    text varchar,

    full_text_vector tsvector generated always as (to_tsvector('english', coalesce(title, '') || ' ' || coalesce(url, '') || ' ' || coalesce(author, '') || ' ' || coalesce(text, ''))) stored
);
create index index_full_text_search_zettels on zettel using gin (full_text_vector);

create table tag (id serial primary key,
    created_at timestamp not null default now(),
    updated_at timestamp,

    name varchar(100) not null,
    slug varchar(100) unique not null
);

create table tag_zettel (
    created_at timestamp not null default now(),
    updated_at timestamp,

    zettel_id integer references zettel not null,
    tag_id integer references tag not null,

    primary key (zettel_id, tag_id)
);
