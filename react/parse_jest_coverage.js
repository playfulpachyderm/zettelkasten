const fs = require("fs");
const parser = require("node-html-parser");

fs.readFile("coverage/lcov-report/index.html", "utf-8", function(err, data) {
  // eslint-disable-next-line no-console
  console.log(parser.parse(data).querySelector(".pad1").querySelector(".strong").text.substr(0, 4));
});
