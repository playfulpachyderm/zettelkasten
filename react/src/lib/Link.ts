interface Link {
  type: "link";
  id: number;
  url: string;
  title: string;
  author?: string;
  text?: string;
}

interface NewLink {
  url: string;
  author?: string;
  text?: string;
}

/**
 * Get the "show" url for a Link
 */
export function url_for(l: Link): string {
  return `/links/${l.id}`;
}

export default Link;
export { NewLink };
