import React from "react";

import Quote from "./Quote";
import Link from "./Link";

import LinkItem from "components/LinkItem";
import QuoteItem from "components/QuoteItem";


/**
 * Return the appropriate component type based on the type of the item
 */
export default function to_item(item: Quote | Link): JSX.Element {
  switch (item.type) {
  case "link":
    return <LinkItem link={item as Link} />;
  case "quote":
    return <QuoteItem quote={item as Quote} />;
  }
  return <div>[Unknown item type]</div>;
}
