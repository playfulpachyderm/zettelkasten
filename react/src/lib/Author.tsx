import React from "react";


/**
 *
 */
export function filtered_url_for(author: string): string {
  const qs = new URLSearchParams({author}).toString();
  return `/quotes?${qs}`;
}

/**
 *
 */
export function filter_by(author: string): void {
  const fakeform = (
    <form action="/quotes" method="get">
      <input type="hidden" name="author" value={author} />
    </form>
  );
  fakeform.props.submit();
}
