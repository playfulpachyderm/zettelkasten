interface Tag {
  id: number;
  name: string;
  slug: string;
}

/**
 * Return the url for the given Tag
 */
export function url_for(tag: Tag): string {
  return `/tags/${tag.slug}`;
}

export default Tag;
