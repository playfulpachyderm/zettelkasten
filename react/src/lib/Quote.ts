import Tag from "./Tag";

interface Quote {
  type: "quote";
  id: number;
  author: string;
  text: string;
  title?: string;
  url?: string;
  tags: Array<Tag>;
}

interface NewQuote {
  author: string;
  text: string;
  title?: string;
  url?: string;
}

/**
 * Generate the url for a Show Quote page
 */
export function url_for(q: Quote): string {
  return `/quotes/${q.id}`;
}

export default Quote;
export { NewQuote };
