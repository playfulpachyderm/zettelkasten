import React from "react";
import { createUseStyles } from "react-jss";

import Quote, { url_for } from "lib/Quote";
import { filtered_url_for } from "lib/Author";

const useStyles = createUseStyles({
  quote_container: {
    display: "flex",
    alignItems: "center",
    padding: "0.5em 0",
  },
  quote_title_and_author: {
    fontStyle: "italic",
    fontSize: "14px",
    padding: "0.2em 2em",
    flex: "0 0 10em",
    color: "#888",
  },
  quote_divider: {
    alignSelf: "stretch",
    borderLeft: "1px solid #aaa",
  },
  quote_text: {
    padding: "0.2em 2em",
    color: "inherit",
  },
});

interface IProps {
  quote: Quote;
}


/**
 * Show a quote
 */
export default function QuoteItem({quote}: IProps): JSX.Element {
  const styles = useStyles();

  let title_and_author;
  if (quote.title)
    title_and_author = `${quote.author}, on ${quote.title}`;
  else
    title_and_author = `${quote.author}`;

  return (
    <div className={styles.quote_container}>
      <a className={styles.quote_title_and_author} href={filtered_url_for(quote.author)}>
        {title_and_author}
      </a>
      <div className={styles.quote_divider} />
      <a className={styles.quote_text} href={url_for(quote)}>{quote.text}</a>
    </div>
  );
}
