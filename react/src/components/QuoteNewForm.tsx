import React, { useState } from "react";
import { createUseStyles } from "react-jss";
import axios from "axios";

import Quote, { url_for, NewQuote } from "lib/Quote";

const useStyles = createUseStyles({
  container: {
    display: "flex",
    flexDirection: "column",
    width: "60em",
    fontSize: "18px",
  },
  input: {
    margin: "0.3em 0",
    fontSize: "inherit",
    padding: "0.3em",
    borderRadius: "0.6em",
    color: "#555",
    ["&::placeholder"]: {
      color: "#999",
    },
  },
  text: {
    height: "35em",
    fontSize: "14px",
    color: "#000",
  },
  submit: {
    width: "min-content",
    boxShadow: "1px 1px 1px 0 #999",
    border: "1px solid #999",
    ["&:active"]: {
      transform: "translate(1px, 1px)",
      boxShadow: "-1px -1px 1px 0 #555",
    },
    ["&:disabled"]: {
      color: "#bbb",
      backgroundColor: "#ddd",
      transform: "none",
      boxShadow: "none",
    },
  },
});


/**
 * Render a form to create a new quote
 */
export default function QuoteNewForm(): JSX.Element {
  const styles = useStyles();

  const [text, setText] = useState("");
  const [author, setAuthor] = useState("");
  const [title, setTitle] = useState("");
  const [url, setUrl] = useState("");

  const is_valid = text && author;

  /**
   * Send form data to API, then redirect to the page for the returned quote
   */
  function submit_form() {
    const form_data: NewQuote = {text, author};
    if (title.length > 0)
      form_data.title = title;
    if (url.length > 0)
      form_data.url = url;
    axios.post("/api/quotes/new", form_data)
      .then(({data}: {data: Quote}) => {
        window.location.href = url_for(data);
      });
  }

  return (
    <div className={styles.container}>
      <textarea
        className={[styles.text, styles.input].join(" ")}
        name="text"
        value={text}
        onChange={e => setText(e.target.value)}
        placeholder="Quote text"
      />
      <input
        className={styles.input}
        name="author"
        value={author}
        onChange={e => setAuthor(e.target.value)}
        placeholder="Author"
      />
      <input
        className={styles.input}
        name="title"
        value={title}
        onChange={e => setTitle(e.target.value)}
        placeholder="Title (optional)"
      />
      <input
        className={styles.input}
        name="url"
        value={url}
        onChange={e => setUrl(e.target.value)}
        placeholder="URL (optional)"
      />
      <button
        name="submit"
        className={[styles.submit, styles.input].join(" ")}
        disabled={!is_valid}
        onClick={submit_form}
      >
        Submit
      </button>
    </div>
  );
}
