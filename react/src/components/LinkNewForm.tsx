import React, { useState } from "react";
import { createUseStyles } from "react-jss";
import axios from "axios";

import { NewLink } from "lib/Link";

const useStyles = createUseStyles({
  container: {
    display: "flex",
    flexDirection: "column",
    width: "60em",
    fontSize: "18px",
  },
  input: {
    margin: "0.3em 0",
    fontSize: "inherit",
    padding: "0.3em",
    borderRadius: "0.6em",
    color: "#555",
    ["&::placeholder"]: {
      color: "#999",
    },
  },
  text: {
    height: "35em",
    fontSize: "14px",
    color: "#000",
  },
  submit: {
    width: "min-content",
    boxShadow: "1px 1px 1px 0 #999",
    border: "1px solid #999",
    ["&:active"]: {
      transform: "translate(1px, 1px)",
      boxShadow: "-1px -1px 1px 0 #555",
    },
    ["&:disabled"]: {
      color: "#bbb",
      backgroundColor: "#ddd",
      transform: "none",
      boxShadow: "none",
    },
  },
});


/**
 * Render a form to create a new link
 */
export default function LinkNewForm(): JSX.Element {
  const styles = useStyles();

  const [text, setText] = useState("");
  const [author, setAuthor] = useState("");
  const [url, setUrl] = useState("");

  const is_valid = url;

  /**
   * Send form data to API, then redirect to the page for the returned link
   */
  function submit_form() {
    const form_data: NewLink = {url};
    if (author.length > 0)
      form_data.author = author;
    if (text.length > 0)
      form_data.text = text;

    axios.post("/api/links/new", form_data)
      .then(() => {
      // .then(({data}: {data: Link}) => {
      //   window.location.href = url_for(data);
        window.location.href = "/links";
      });
  }

  return (
    <div className={styles.container}>
      <input
        className={styles.input}
        name="url"
        value={url}
        onChange={e => setUrl(e.target.value)}
        placeholder="URL"
      />
      <textarea
        className={[styles.text, styles.input].join(" ")}
        name="text"
        value={text}
        onChange={e => setText(e.target.value)}
        placeholder="Description"
      />
      <input
        className={styles.input}
        name="author"
        value={author}
        onChange={e => setAuthor(e.target.value)}
        placeholder="Author (optional)"
      />
      <button
        name="submit"
        className={[styles.submit, styles.input].join(" ")}
        disabled={!is_valid}
        onClick={submit_form}
      >
        Submit
      </button>
    </div>
  );
}
