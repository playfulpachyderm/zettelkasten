import React from "react";
import { createUseStyles } from "react-jss";

import Link, { url_for } from "lib/Link";
import colors from "colors";

const useStyles = createUseStyles({
  link_container: {
    display: "block",
    width: "70em",
    border: "1px solid #ddd",
    margin: "0.5em",
    padding: "0.3em 0.6em",
    borderRadius: "0.3em",
  },
  link_title: {
    color: "inherit",
    margin: "0",
  },
  title_divider: {
    padding: "0 0.5em",
  },
  url: {
    fontSize: "12px",
    color: colors.primary,
  },
  link_description: {
    margin: "0",
    fontSize: "10px",
    color: "#888",
  },
});

interface IProps {
  link: Link;
}


/**
 * List a link item
 */
export default function LinkItem({link}: IProps): JSX.Element {
  const styles = useStyles();

  return (
    <div className={styles.link_container}>
      <div>
        <a className={styles.link_title} href={url_for(link)}>{link.title}</a>
        <span className={styles.title_divider}>|</span>
        <a className={styles.url} href={link.url} target="_blank" rel="noreferrer">{link.url}</a>
      </div>
      <p className={styles.link_description}>{link.text}</p>
    </div>
  );
}
