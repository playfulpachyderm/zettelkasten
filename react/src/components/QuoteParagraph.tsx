import React from "react";

/**
 * One paragraph of a quote.  Helps by introducing line breaks (`<br />`) where necessary.
 */
export default function QuoteParagraph({p}: {p: string}): JSX.Element {
  const lines = p.split("\n");
  const split_lines: Array<string | JSX.Element> = [lines[0]];
  for (let i = 1; i < lines.length; ++i) {
    split_lines.push(<br key={i} />);
    split_lines.push(lines[i]);
  }
  return <p>{split_lines}</p>;
}
