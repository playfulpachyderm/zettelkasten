import React from "react";
import { createUseStyles } from "react-jss";

import Quote from "lib/Quote";
import { url_for } from "lib/Tag";
import QuoteParagraph from "components/QuoteParagraph";
import { filtered_url_for } from "lib/Author";

const useStyles = createUseStyles({
  quote_container: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    height: "90vh",
    width: "80%",
  },
  quote_content_container: {
    position: "relative",
    padding: "0 5em",
  },
  background_quotation: {
    opacity: "0.1",
    height: "10em",
    position: "absolute",
    zIndex: "-1",
    transform: "translate(-30%, -70%)",
  },
  text: {
    fontSize: "20px",
  },
  author_container: {
    textAlign: "right",
    color: "#888",
    padding: "1em 5em",
  },
  author: {
    color: "inherit",
  },
  title: {
    padding: "0.4em 0em",
  },

  tags_container: {
    padding: "0.8em",
    border: "1px solid hsl(50, 38%, 80%)",
    borderRadius: "0.8em",
    backgroundColor: "hsl(50, 80%, 95%)",
    color: "hsl(50, 12%, 40%)",
    display: "flex",
    justifyContent: "center",
  },
  tag: {
    paddingLeft: "0.8em",
    ["&:nth-child(n+2)::before"]: {
      content: "'\\00b7'f",
      paddingRight: "0.8em",
    },
  },
});

interface IProps {
  quote: Quote;
}


/**
 * Render a quote page
 */
export default function QuotePage({quote}: IProps): JSX.Element {
  const styles = useStyles();

  return (
    <div className={styles.quote_container}>
      <div className={styles.quote_content_container}>
        <img className={styles.background_quotation} src="/img/quotation_marks.png" />
        <div className={styles.text}>
          { quote.text.split("\n\n").map(p => <QuoteParagraph key={p} p={p} />) }
        </div>
        <div className={styles.author_container}>
          <a className={styles.author} href={filtered_url_for(quote.author)}>
            ~
            {quote.author}
          </a>
          { quote.title && (
            <div className={styles.title}>
              from:&nbsp;
              {quote.title}
            </div>
          )}
        </div>
      </div>
      <div className={styles.tags_container}>
        {quote.tags.map(tag => (
          <span key={tag.id} className={styles.tag}>
            <a href={url_for(tag)}>{tag.name}</a>
          </span>
        ))}
      </div>
    </div>
  );
}
