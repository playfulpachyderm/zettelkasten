import React from "react";
import ReactDOM from "react-dom";

import QuoteNewForm from "components/QuoteNewForm";

document.addEventListener("DOMContentLoaded", () => {
  ReactDOM.render(<QuoteNewForm />, document.getElementById("app"));
});
