import React from "react";
import ReactDOM from "react-dom";

import axios from "axios";

import QuotePage from "components/QuotePage";


axios.get(`/api${ window.location.pathname}`)
  .then(({data}) => {
    ReactDOM.render(
      <QuotePage quote={data} />,
      document.getElementById("app")
    );
  });
