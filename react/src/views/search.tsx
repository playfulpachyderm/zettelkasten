import React from "react";
import ReactDOM from "react-dom";
import { createUseStyles } from "react-jss";

import axios from "axios";

import to_item from "lib/Zettel";
import Quote from "lib/Quote";
import Link from "lib/Link";


interface IProps {
  items: Array<Quote | Link>;
}

const useStyles = createUseStyles({
  result_count: {
    fontStyle: "italic",
    fontSize: "18px",
    color: "hsl(50, 38%, 57%)",
  },
});

/**
 * Render a search results page
 */
function SearchPage(props: IProps) {
  const styles = useStyles();
  return (
    <>
      <h1>Search results</h1>
      <h2 className={styles.result_count}>{`-- Found ${props.items.length} zettels --`}</h2>
      {
        props.items.map(to_item)
      }
    </>
  );
}

axios.get(`/api${ window.location.pathname}${window.location.search}`)
  .then(({data}) => {
    ReactDOM.render(
      <SearchPage items={data} />,
      document.getElementById("app")
    );
  });
