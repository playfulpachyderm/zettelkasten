import React from "react";
import ReactDOM from "react-dom";

import axios from "axios";

import Link from "lib/Link";
import LinkItem from "components/LinkItem";


axios.get(`/api/links${ window.location.search}`)
  .then(({data}: {data: Link[]}) => {
    ReactDOM.render(
      data.map(l => <LinkItem key={l.id} link={l} />),
      document.getElementById("app")
    );
  });
