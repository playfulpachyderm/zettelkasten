import React from "react";
import ReactDOM from "react-dom";

import LinkNewForm from "components/LinkNewForm";

document.addEventListener("DOMContentLoaded", () => {
  ReactDOM.render(<LinkNewForm />, document.getElementById("app"));
});
