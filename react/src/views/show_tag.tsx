import ReactDOM from "react-dom";

import axios from "axios";

import to_item from "lib/Zettel";

axios.get(`/api${ window.location.pathname}?with-zettels`)
  .then(({data}) => {
    ReactDOM.render(data.zettels.map(to_item),
      document.getElementById("app")
    );
  });
