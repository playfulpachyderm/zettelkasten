import React from "react";
import ReactDOM from "react-dom";

import axios from "axios";

import Quote from "lib/Quote";
import QuoteItem from "components/QuoteItem";


axios.get(`/api/quotes${ window.location.search}`)
  .then(({data}: {data: Quote[]}) => {
    ReactDOM.render(
      data.map(q => <QuoteItem key={q.id} quote={q} />),
      document.getElementById("app")
    );
  });
