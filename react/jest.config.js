const path = require('path');

module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  moduleDirectories: ["src", __dirname, "node_modules"],
  testMatch: [path.join(__dirname, "test/**/?(*.)+(spec|test).[jt]s?(x)")],
  transform: {
    '^.+\\.[jt]sx$': 'babel-jest',
  },
};
