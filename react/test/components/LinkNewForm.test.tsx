import React from "react";
import { create, act } from "react-test-renderer";

// import QuoteNewForm from "components/QuoteNewForm";
import LinkNewForm from "components/LinkNewForm";

import { set_text } from "test/test_utils";


describe("LinkNewForm component", () => {
  const component = create(<LinkNewForm />);
  const dom_root = component.root;
  const submit_button = dom_root.findByProps({name: "submit"});
  const url_field = dom_root.findByProps({name: "url"});

  test("Should render", () => {
    const all_inputs = dom_root.findAll(x => (x.props.className || "").split(" ").includes("input"));
    expect(all_inputs).toHaveLength(4);
  });

  test("Submit button should be disabled", () => {
    expect(submit_button.props.disabled).toBe(true);
  });

  test("Submit button should be enabled if URL is given", () => {
    act(() => set_text(url_field, "fwewf"));
    expect(submit_button.props.disabled).toBe(false);
  });
});
