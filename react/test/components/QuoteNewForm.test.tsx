import React from "react";
import { create, act } from "react-test-renderer";

import QuoteNewForm from "components/QuoteNewForm";

import { set_text } from "test/test_utils";


describe("QuoteNewForm component", () => {
  const component = create(<QuoteNewForm />);
  const dom_root = component.root;
  const submit_button = dom_root.findByProps({name: "submit"});
  const text_field = dom_root.findByProps({name: "text"});
  const author_field = dom_root.findByProps({name: "author"});

  test("Should render", () => {
    const all_inputs = dom_root.findAll(x => (x.props.className || "").split(" ").includes("input"));
    expect(all_inputs).toHaveLength(5);
  });

  test("Submit button should be disabled", () => {
    expect(submit_button.props.disabled).toBe(true);

    // Text only
    act(() => set_text(text_field, "fawef"));
    expect(submit_button.props.disabled).toBe(true);

    // Author only
    act(() => set_text(text_field, ""));
    act(() => set_text(author_field, "afwjekl"));
    expect(submit_button.props.disabled).toBe(true);
  });

  test("Submit button should be enabled if text an author are given", () => {
    act(() => set_text(text_field, "fwe"));
    act(() => set_text(author_field, "fwewf"));
    expect(submit_button.props.disabled).toBe(false);
  });
});
