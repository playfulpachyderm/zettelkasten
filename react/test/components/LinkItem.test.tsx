import React from "react";
import { create, ReactTestInstance } from "react-test-renderer";

import Link from "lib/Link";
import LinkItem from "components/LinkItem";


describe("LinkItem component", () => {
  const link: Link = {type: "link", id: 1, url: "http://somelink", title: "Some Link", text: "fwjefkj"};
  const component = create(<LinkItem link={link} />);
  const dom_root = component.root;
  const container = dom_root.findByProps({className: "link_container"});

  test("renders", () => {
    expect(container).not.toBeNull();

    const link_title_a = container.findByProps({className: "link_title"}) as ReactTestInstance;
    expect(link_title_a.children[0]).toBe(link.title);

    const url_a = container.findByProps({className: "url"}) as ReactTestInstance;
    expect(url_a.children[0]).toBe(link.url);

    const description_p = container.findByProps({className: "link_description"}) as ReactTestInstance;
    expect(description_p.children[0]).toBe(link.text);
  });
});
