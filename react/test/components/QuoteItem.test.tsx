import React from "react";
import { create, ReactTestInstance } from "react-test-renderer";

import Quote from "lib/Quote";
import QuoteItem from "components/QuoteItem";


describe("QuoteItem component with no title", () => {
  const quote: Quote = {id: 1, type: "quote", author: "Asdf", text: "jklsemicolon", tags: []};
  const component = create(<QuoteItem quote={quote} />);
  const dom_root = component.root;
  const div = dom_root.findByType("div");

  test("renders", () => {
    const children: Array<ReactTestInstance> = div.children as Array<ReactTestInstance>;
    expect(children).toHaveLength(3);
    expect(children[0].children[0]).toBe(quote.author);
    expect(children[2].children[0]).toBe(quote.text);
  });
});


describe("QuoteItem component with a title", () => {
  const quote: Quote = {id: 1, type: "quote", author: "Asdf", text: "jklsemicolon", title: "potatoes", tags: []};
  const component = create(<QuoteItem quote={quote} />);
  const dom_root = component.root;
  const div = dom_root.findByType("div");

  test("renders", () => {
    const children: Array<ReactTestInstance> = div.children as Array<ReactTestInstance>;
    expect(children).toHaveLength(3);
    expect(children[0].children[0]).toBe("Asdf, on potatoes");
    expect(children[2].children[0]).toBe(quote.text);
  });
});
