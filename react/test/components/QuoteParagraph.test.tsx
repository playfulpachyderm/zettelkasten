import React from "react";
import { create, ReactTestInstance } from "react-test-renderer";

import QuoteParagraph from "components/QuoteParagraph";

describe("QuoteParagraph component", () => {
  const text = "asdf";
  const component = create(<QuoteParagraph p={text} />);
  const dom_root = component.root;

  test("renders", () => {
    expect(dom_root.findByType("p")).not.toBeNull();
    expect(dom_root.findByType("p").children).toHaveLength(1);
    expect(dom_root.findByType("p").children[0]).toBe(text);
  });
});

describe("QuoteParagraph component", () => {
  const text = "asdf\nAsdf2\nAsdf3";
  const component = create(<QuoteParagraph p={text} />);
  const dom_root = component.root;

  test("renders", () => {
    expect(dom_root.findByType("p")).not.toBeNull();
    expect(dom_root.findByType("p").children).toHaveLength(5);

    expect(dom_root.findByType("p").children[0]).toBe("asdf");
    expect((dom_root.findByType("p").children[1] as ReactTestInstance).type).toBe("br");
    expect(dom_root.findByType("p").children[2]).toBe("Asdf2");
    expect((dom_root.findByType("p").children[3] as ReactTestInstance).type).toBe("br");
    expect(dom_root.findByType("p").children[4]).toBe("Asdf3");
  });
});
