import React from "react";
import { create, ReactTestInstance } from "react-test-renderer";

import Quote from "lib/Quote";
import QuotePage from "components/QuotePage";


describe("QuotePage component with no title", () => {
  const quote: Quote = {id: 1, type: "quote", author: "Asdf", text: "jklsemicolon", tags: []};
  const component = create(<QuotePage quote={quote} />);
  const dom_root = component.root;

  test("renders", () => {
    const text_div = dom_root.findByProps({className: "text"});
    expect(text_div.children).toHaveLength(1);
    expect((text_div.children[0] as ReactTestInstance).props.p).toBe(quote.text);

    const author_div = dom_root.findByProps({className: "author"});
    expect(author_div.children).toHaveLength(2);
    expect(author_div.children[1]).toBe(quote.author);

    expect(dom_root.findAllByProps({className: "title"})).toHaveLength(0);
  });
});

describe("QuotePage component with multiline quote and title", () => {
  const quote_text = "Something!\n\nSomething else...\n\nA line with\nmultiple\nlines in it";
  const quote: Quote = {id: 1, type: "quote", author: "Asdf", text: quote_text, title: "potatoes", tags: []};
  const component = create(<QuotePage quote={quote} />);
  const dom_root = component.root;

  test("renders", () => {
    const text_div = dom_root.findByProps({className: "text"});
    expect(text_div.children).toHaveLength(3);
    expect((text_div.children[0] as ReactTestInstance).props.p).toBe("Something!");
    expect((text_div.children[1] as ReactTestInstance).props.p).toBe("Something else...");
    expect((text_div.children[2] as ReactTestInstance).props.p).toBe("A line with\nmultiple\nlines in it");

    const author_div = dom_root.findByProps({className: "author"});
    expect(author_div.children).toHaveLength(2);
    expect(author_div.children[1]).toBe(quote.author);

    expect(dom_root.findAllByProps({className: "title"})).toHaveLength(1);
    expect(dom_root.findByProps({className: "title"}).children).toHaveLength(2);
    expect(dom_root.findByProps({className: "title"}).children[1]).toBe(quote.title);
  });
});
