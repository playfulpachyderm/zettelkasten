/**
 * Some helpful testing functions to easily test input elements on the fake
 * DOM.
 */
import { ReactTestInstance } from "react-test-renderer";


/**
 * Set the text on an input element
 */
function set_text(element: ReactTestInstance, text: string): void {
  element.props.onChange({target: { value: text }});
}

/**
 * Run an `expect` assertion on the text of an input elemtn
 */
function expect_text(element: ReactTestInstance, text: string): void {
  expect(element.props.value).toBe(text);
}

/**
 * Assert equivalence between objects
 */
function expect_json_equivalent(o1: Record<string, unknown>, o2: Record<string, unknown>): void {
  expect(JSON.stringify(o1)).toEqual(JSON.stringify(o2));
}

export { set_text, expect_text, expect_json_equivalent };
