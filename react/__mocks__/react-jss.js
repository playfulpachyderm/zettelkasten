/**
 * Mock out the `createUseStyles` function in `react-jss` package.
 */


/**
 * Return a [function that returns a] new object that has the same keys.  These keys
 * will function as the generated class names (otherwise they are `undefined`).  Then
 * we can assert that dynamically created JSX elements are given the right class names.
 */
module.exports.createUseStyles = stuff => {
  const ret = {};
  for (const i in stuff)
    if ({}.hasOwnProperty.call(stuff, i))
      ret[i] = i;
  return () => ret;
};
