// We are using node's native package 'path'
// https://nodejs.org/api/path.html
const path = require('path');

// Constant with our paths
const paths = {
  DIST: path.resolve(__dirname, '..', 'static', 'js'),
  SRC: path.resolve(__dirname, 'src'),
};

// Webpack configuration
module.exports = {
  resolve: {
    // Resolve imports by looking in these base paths
    modules: [paths.SRC, 'node_modules'],
    // Look for files with these extensions
    extensions: [".js", ".jsx", ".ts", ".tsx"],
  },

  entry: {
    index_quotes: path.join('views', "index_quotes.tsx"),
    show_quote: path.join('views', "show_quote.tsx"),
    new_quote: path.join('views', "new_quote.tsx"),

    index_links: path.join('views', "index_links.tsx"),
    new_link: path.join('views', "new_link.tsx"),

    show_tag: path.join('views', "show_tag.tsx"),

    search: path.join('views', "search.tsx"),
  },
  output: {
    path: paths.DIST,
    filename: '[name].js',
  },
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.([tj]sx?)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
    ],
  },
};
