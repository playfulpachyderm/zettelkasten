#!/bin/bash

set -e
set -x

# Get the access token, either by command-line arg (for local testing) or from the sr.ht secret
# ---------------------------------------------------------------------------------------------

gitlab_access_token=$1  # for testing locally
if [[ -z $gitlab_access_token ]]; then
    # If none provided, use the sr.ht secret file
    gitlab_access_token=$(cat ~/gitlab_access_token)
fi
if [[ -z $gitlab_access_token ]]; then
    # Can't proceed without a token
    >&2 echo "No access token!"
    exit 1
fi

gitlab_badges_base_url="https://gitlab.com/api/v4/projects/24512868/badges"
gitlab_pytest_badge_id=127683


# Flask tests
# -----------

percent_covered=$(jq .totals.percent_covered zettelkasten/coverage.json | xargs printf "%.1f")

if [[ $(echo "$percent_covered > 95" | bc) -eq "1" ]]; then
    color=brightgreen
elif [[ $(echo "$percent_covered > 90" | bc) -eq "1" ]]; then
    color=yellow
else
    color=red
fi

image_url="https://img.shields.io/badge/pytest%20coverage-$percent_covered%2525-$color"

curl --request PUT -H "PRIVATE-TOKEN: $gitlab_access_token" --data "link_url=https://playfulpachyderm.com&image_url=$image_url" $gitlab_badges_base_url/$gitlab_pytest_badge_id
